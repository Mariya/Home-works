/*
* В большинстве случаев внешний вид элементов задаётся классами.
* А JavaScript добавляет или удаляет их. Такой код красив и гибок, дизайн можно легко изменять.
Свойство style нужно использовать лишь там, где классы не подходят,
например если точное значение цвета/отступа/высоты вычисляется в JavaScript.
* */


; let menuBurger = document.getElementsByClassName("navbar-block-link-burger")[0]; //
let menuCross = document.getElementsByClassName("navbar-block-link-cross")[0]; //
let mobileMenu = document.getElementById("navbar-menu-block");

menuBurger.addEventListener("click", function (event) { // вешаем обработчик на меню
    let target = event.target;
    menuBurger.setAttribute("style", `display: none`);
    mobileMenu.setAttribute("style", `display: block`);
    menuCross.setAttribute("style", `display: block`);
});

menuCross.addEventListener("click", function (event) { // вешаем обработчик на меню
    let target = event.target;
    menuCross.setAttribute("style", `display: none`);
    mobileMenu .setAttribute("style", `display: none`);
    menuBurger.setAttribute("style", `display: block`);
});

