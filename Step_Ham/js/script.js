// Services section function

$(function () {
    let servicesMenuLink = $(".services-section-menu-link");

    servicesMenuLink.on("click", (function () {
        $(".services-section-menu-link.active").removeClass("active");
        $(this).addClass("active");
        let infoToShow = $(this).attr("data-webId");
        $(".services-section-menu-info_active").slideDown('slow',function() {
            $(this).removeClass("services-section-menu-info_active");
            $("#" + infoToShow).slideUp(function () {
                $(this).addClass("services-section-menu-info_active");
            });
        });
    }))
});

// Portfolio section

$(function () {
    let portfolioMenuLink = $(".portfolio-section-menu-link");
    portfolioMenuLink.click( function () {
        $(".portfolio-section-menu-link.active").removeClass("active");
        $(this).addClass("active");

        // determines which tab of the menu is clicked
        let imageToShow = $(this).attr("data-filter");
        let filter = $(".portfolio-section-gallery-category.filter");
        // if the picture class match to the menu tab 'data-webId' attribute value 'ALL', then all pictures are shown
        if (imageToShow === "portfolio-all") {
            filter.show();

        } else {
            // if the picture class doesn't match to the menu tab 'data-webId' attribute, then the picture is hidden
            filter.not("." + imageToShow).hide();
            // if the picture class match to the menu tab 'data-webId' attribute, then the picture is shown
            filter.filter("." + imageToShow).show();
        }
    });
});


// Portfolio section - imitation of server work

$(function () {
    let galleryButton = $(".section-btn");
    let hideImages = $(".portfolio-section-gallery-category.hide");
    let bubblesAnimation = $(".bubbles");

    galleryButton.click(function () {
        galleryButton.hide(); // button hiding
        bubblesAnimation.css("display", "block"); // animation showing
        setTimeout(function () {
            bubblesAnimation.fadeOut(300); // animation hiding after 2s and with the delay - 300ms
        }, 2000);

        setTimeout(function(){
            let images = hideImages.removeClass("hide").addClass("filter");
            images.show();
        }, 2000);
        galleryButton.hide();
    })
});

// Navbar fixation function

$(function () {
    $("#navbar").removeClass("fixed");
    $(window).scroll(function () {
        if ($(this).scrollTop () > 119) {
            $("#navbar").addClass("fixed").fadeIn();
        } else {
            $("#navbar").removeClass("fixed").fadeIn();
        }
    })
});

// Navbar scrolling function

$(function () {
    let menuLink = $(".menu-top-item");
    menuLink.on("click", "a", function (event) {
        event.preventDefault();
        let target = $(this).attr("href"),
            top = $(target).offset().top - 50;
        console.log(top);
        $('html, body').animate({
            scrollTop: top
        }, 1000);
        return false;
    });
});



$(function () {
    $("#prev").click(function () {

        // finding the index of active small photo in carousel
        let activeSmallPhoto = $(".carousel-small-photo-active");
        let activeSmallPhotoIndex = activeSmallPhoto.index();

        let reviewItem = $(".review-item");
        // if index of $reviewItem equals to $activeSmallPhotoIndex than hide $reviewItem
        reviewItem.eq(activeSmallPhotoIndex).hide();
        activeSmallPhoto.removeClass("carousel-small-photo-active");

        // if index of $activeSmallPhoto equals to it's index - 1, than add class active;
        let smallPhoto = $(".carousel-small-photo");
        smallPhoto.eq(activeSmallPhotoIndex - 1).addClass("carousel-small-photo-active");

        // if $reviewItem equals to $activeSmallPhotoIndex - 1, than show this $reviewItem
        reviewItem.eq(activeSmallPhotoIndex - 1).fadeIn();
    });

    $(".carousel-small-photo").click(function () {
        // finding the clicked small photo in carousel
        let activeSmallPhoto = $(this);

        // if small photo is active, than remove it's active class
        $(".carousel-small-photo-active").removeClass("carousel-small-photo-active");
        activeSmallPhoto.addClass("carousel-small-photo-active");

        let activeSmallPhotoData = activeSmallPhoto.attr("data-tab");
        let reviewItem = $(".review-item");
        // if the $reviewItem class doesn't match to the menu tab 'data-webId' attribute, then the picture is hidden
        reviewItem.not("." + activeSmallPhotoData).hide();
        // if the picture class match to the menu tab 'data-webId' attribute, then the picture is shown
        reviewItem.filter("." + activeSmallPhotoData).show();
    });

    $('#next').click(function () {

        let smallPhoto = $(".carousel-small-photo");
        let activeSmallPhoto = $(".carousel-small-photo-active");
        // finding the index of active small photo in carousel
        let activeSmallPhotoIndex = activeSmallPhoto.index();

        let reviewItem = $(".review-item");

        // if index of $reviewItem equals to $activeSmallPhotoIndex than hide $reviewItem
        reviewItem.eq(activeSmallPhotoIndex).hide();

        // if $activeSmallPhotoIndex equals to $smallPhoto length - 1 than =>
        // $activeSmallPhotoIndex is equals to -1. In other case =>
        // $activeSmallPhotoIndex equals to itself
        activeSmallPhotoIndex = activeSmallPhotoIndex === smallPhoto.length - 1 ? -1 : activeSmallPhotoIndex;
        activeSmallPhoto.removeClass("carousel-small-photo-active");
        smallPhoto.eq(activeSmallPhotoIndex + 1).addClass("carousel-small-photo-active");
        reviewItem.eq(activeSmallPhotoIndex + 1).fadeIn();
    });
});






